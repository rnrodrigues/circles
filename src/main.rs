use std::io;

fn main() {
    let mut radius = String::new();
    
    println!("Input radius.");

    io::stdin()
        .read_line(&mut radius)
        .expect("Failed to read line");

    let radius: f64 = radius
        .trim()
        .parse()
        .expect("Please type a number");

    const PI: f64 = 3.1415;
    
    let diameter = radius * 2.0;
    let circumference = diameter * PI;
    let area = PI * radius * radius;

    println!("       radius = {radius:.3}");
    println!("     diameter = {diameter:.3}");
    println!("circumference = {circumference:.3}");
    println!("         area = {area:.3}");
}
